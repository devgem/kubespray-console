FROM ubuntu:20.10

ARG VCS_REF
ARG VERSION
ARG BUILD_DATE

LABEL \
    org.label-schema.schema-version="1.0" \
    org.label-schema.name="kubespray-console" \
    org.label-schema.description="Based on kubespray, all tools to deploy a production-grade Kubernetes cluster in one convenient container." \
    org.label-schema.vcs-url="https://gitlab.com/devgem/kubespray-console" \
    org.label-schema.vendor="DevGem" \
    org.label-schema.vcs-ref=$VCS_REF \
    version=$VERSION \
    build-date=$BUILD_DATE


# Install system stuff
RUN \
    apt-get update && \
    apt-get install git -y && \
    apt-get install python3-pip -y && \
    apt-get install sshpass

# Clone kubespray
RUN  \
    cd ~ && \
    git clone https://github.com/kubernetes-sigs/kubespray.git && \
    pip install -r ./kubespray/requirements.txt

# Pepare folders and script
COPY init-cluster-config /usr/local/bin
COPY deploy-cluster /usr/local/bin
COPY update-cluster /usr/local/bin
RUN \
    mkdir ~/kubespray/inventory/clusters && \
    chmod u+x /usr/local/bin/init-cluster-config && \
    chmod u+x /usr/local/bin/deploy-cluster && \
    chmod u+x /usr/local/bin/update-cluster

# Display versions
RUN \
    echo '' && \
    echo '********' && \
    echo 'VERSIONS' && \
    echo '********' && \
    echo '' && \
    echo 'kubespray:' && \
    cd ~/kubespray && \
    git rev-parse --short HEAD && \
    echo '' && \
    echo 'ansible:' && \
    ansible --version && \
    echo '' && \
    echo '********' && \
    echo ''
