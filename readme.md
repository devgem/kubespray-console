# kubespray-console

[![kubespray](https://img.shields.io/badge/kubespray-99b8f090-brightgreen.svg)](https://github.com/kubernetes-sigs/kubespray)
 [![ansible](https://img.shields.io/badge/ansible-2.9.6-brightgreen.svg)](https://github.com/ansible/ansible)

[![Docker Stars](https://img.shields.io/docker/stars/devgem/kubespray-console.svg)](https://hub.docker.com/r/devgem/kubespray-console 'DockerHub')
 [![Docker Pulls](https://img.shields.io/docker/pulls/devgem/kubespray-console.svg)](https://hub.docker.com/r/devgem/kubespray-console 'DockerHub')
 [![Docker Layers](https://images.microbadger.com/badges/image/devgem/kubespray-console.svg)](https://microbadger.com/images/devgem/kubespray-console)
 [![Docker Version](https://images.microbadger.com/badges/version/devgem/kubespray-console.svg)](https://hub.docker.com/r/devgem/kubespray-console)

This docker image contains [`kubespray`](https://kubespray.io/) and [`ansible`](https://www.ansible.com/) and some facilitation scripts.

It allows you to set up a [`kubernetes`](https://kubernetes.io/) cluster with minimum effort.

For the cluster you will need some linux machines (in the cloud or on-premise) and credentials or a private key to access them. The supported linux distributions can be found in the [kubespray documentation](https://kubespray.io/#/?id=supported-linux-distributions).

To build the image:

```shell
export KUBESPRAY_CONSOLE_VERSION="0.5"
docker build -t devgem/kubespray-console:$KUBESPRAY_CONSOLE_VERSION --build-arg VERSION="$KUBESPRAY_CONSOLE_VERSION" --build-arg VCS_REF=`git rev-parse --short HEAD` --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` .
```

## Usage

### Start the kubespray-console container

```bash
# create a folder on the host machine, make sure the fodlder is accessible by the container by setting the appropriate rights
mkdir c:\kubespray

# start the container with the mapped host folder and open the console of the container
docker run --rm -it -v //c/kubespray:/root/kubespray/inventory/clusters devgem/kubespray-console:0.5 bash
```

### Prepare a cluster configuration named `my-cluster`

(in this sample the cluster is named `my-cluster`, but you can name it differently)

```bash
# inside of the container's console, initialize a cluster named my-cluster
init-cluster-config my-cluster
```

The mapped host folder should now contain a folder named `my-cluster`. Inside this folder the following files need to be reviewed:

* `inventory.ini`
* `group_vars/k8s-cluster/k8s-cluster.yml`
* `group_vars/k8s-cluster/addons.yml`

The `.yml` files should be self explanatory.

For the `inventory.ini` file, see the [kubespray documentation](https://kubespray.io/#/docs/ansible).

For an explanation of the variables, see the [kubespray documentation](https://kubespray.io/#/docs/vars).

If you are using a key instead of username/password to access the cluster nodes, then kubespray-console expects the key to access the linux machines to:

* be in a `keys` folder (in our sample the key would be found at `my-cluster/keys/kubespray.pem`),
* have no password,
* be in the `.pem` format,
* be called `kubespray.pem`
* and be defined in `inventory.ini` as `~/kubespray/kubespray.pem`, e.g:

```yaml
node1 ansible_host=167.71.8.73 etcd_member_name=etcd1 ansible_ssh_private_key_file=~/kubespray/kubespray.pem
```

(replace `167.71.8.73` with the ip address of your node)

Since the `kubespray.pem` file has to have the correct file rights the `.pem` file will be copied to the `~/kubespray` folder by the `apply-cluster` script and that script will set the approriate rights (this copying is because there are problems with setting the file rights if the file is located in a host folder on Windows).

### Deploy the cluster named `my-cluster`

```bash
# inside of the container's console, deploy the cluster named my-cluster
deploy-cluster my-cluster
```

When deployment is finished you can find the kubectl config file in one of the master nodes at `/etc/kubernetes/admin.conf`. Copy the `admin.config` file to a machine from where you want to access the cluster via kubectl.

```bash
scp -i keys/kubespray.pem root@167.71.8.73:/etc/kubernetes/admin.conf .
```

(replace `167.71.8.73` with the ip address of your first master node)

You can check the cluster externally with:

```bash
kubectl --kubeconfig=admin.conf get nodes
```

If you did not install the dashboard in the `k8s/addons.yml` you can still install it:

```bash
kubectl --kubeconfig=admin.conf create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
```

You will need to create a user and retrieve the user's token to access the dashboard:

```bash
kubectl --kubeconfig=admin.conf create serviceaccount dashboard -n default
kubectl --kubeconfig=admin.conf create clusterrolebinding dashboard-admin -n default --clusterrole=cluster-admin  --serviceaccount=default:dashboard
kubectl --kubeconfig=admin.conf get secret $(kubectl  --kubeconfig=admin.conf get serviceaccount dashboard -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode
```

Now that you have the token you can start the proxy:

```bash
kubectl --kubeconfig=admin.conf proxy
```

And access the dashboard with the token via <http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/>

### Change the cluser

To change the cluster just edit the configuration and redeploy the cluster.

### Update the cluster

The container contains a script to [gracefully update](https://kubespray.io/#/docs/upgrades?id=graceful-upgrade) the Kubernetes version (it is advised to only update 1 minor version at a time, e.g. from 1.17.0 to 1.18.8).

```bash
# inside of the container's console, update the Kubernetes version of my-cluster to v1.18.8
update-cluster my-cluster v1.18.8
```

## Versions

Pre-built images are available on Docker Hub:

| image | date | kubespray | ansible | remarks
|-----|-----|-----|-----|-----|
| [`devgem/kubespray-console:0.5`](https://hub.docker.com/r/devgem/kubespray-console/tags/) | 20201007 | [99b8f090](https://github.com/kubernetes-sigs/kubespray/commit/99b8f090) | 2.9.6 | Updated kubespray.. |
| [`devgem/kubespray-console:0.4`](https://hub.docker.com/r/devgem/kubespray-console/tags/) | 20200826 | [1ff95e85](https://github.com/kubernetes-sigs/kubespray/commit/1ff95e85) | 2.9.6 | Added `update-cluster` script. |
| [`devgem/kubespray-console:0.3`](https://hub.docker.com/r/devgem/kubespray-console/tags/) | 20200826 | [1ff95e85](https://github.com/kubernetes-sigs/kubespray/commit/1ff95e85) | 2.9.6 | Updated ubuntu, kubespray and ansible. |
| [`devgem/kubespray-console:0.2`](https://hub.docker.com/r/devgem/kubespray-console/tags/) | 20190605 | [818aa7ae](https://github.com/kubernetes-sigs/kubespray/commit/818aa7ae) | 2.7.8 | |
| [`devgem/kubespray-console:0.1`](https://hub.docker.com/r/devgem/kubespray-console/tags/) | 20190605 | 2.10 | 2.7.8 | |

## Attributions

* <https://kubespray.io>
* <https://dzone.com/articles/kubernetes-113-installation-using-kubespray>
* <https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/create-with-putty/>
